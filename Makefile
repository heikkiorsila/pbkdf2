CC = gcc
CFLAGS = -W -Wall -O2 -g
LDFLAGS = -lgcrypt
STATICLDFLAGS = -lgpg-error

all:	pbkdf2

pbkdf2:	pbkdf2.o
	$(CC) -o $@ $< $(LDFLAGS)

static:	pbkdf2.o
	$(CC) -static -o pbkdf2-static $< $(LDFLAGS) $(STATICLDFLAGS)

clean:	
	rm -f *.o pbkdf2 pbkdf2-static

%.o:	%.c
	$(CC) $(CFLAGS) -c $<
