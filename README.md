# Introduction

**pbkdf2** generates SHA-256 hashes on command-line.
It is useful for generating crypto keys with relatively short passphrases.
It uses [pbkdf2 algorithm](https://en.wikipedia.org/wiki/PBKDF2) that has
a parameter that controls how long it takes to compute one hash. By default,
**pbkdf2** uses a million iterations.

For example, on AMD Ryzen Threadripper 1950X, using **pbkdf2** with a million
iterations, it takes approx 0.6s to try one password. Trying all 12 character
passwords from [a-zA-Z0-9] would take approximately 6e13 computation years
with a single thread by following this calculation with Python:
```
>>> '{:e}'.format(pow(62,12) * 0.6 / (3600 * 24 * 365))
'6.138255e+13'
```

# Requirements for Debian

```
$ sudo apt-get install libgpg-error-dev libgcrypt20-dev
```

# Installation

0. Build binary:
```
$ make
```

1. Install the binary:
```
$ sudo install pbkdf2 /usr/local/bin/
```

# Usage
Run **pbkdf2** without arguments to see available options:
```
$ pbkdf2
...
USAGE:

    pbkdf2 [-i ITERATIONS] [-p] [-r] SALTFILE

where

 -i    Set number of iterations for pbkdf2.
       The default is 1000000 (million) iterations.
       The higher the value the longer it takes to compute one hash.
 -p    Set passthrough mode. Password is first read from /dev/tty, and the
       password is written to stdout. Then, all data from stdin is read and
       written to stdout. You can use this option to give a password for
       gpg. See EXAMPLE section.
 -r    Set raw mode. Key is printed to stdout in raw
       (non-hexadecimal ascii) format. By default, key is printed in
       hexadecimal format.
...
```

# Examples

To test output of **pbkdf2**, run:
```
$ dd if=/dev/urandom of=saltfile bs=64 count=1
$ pbkdf2 saltfile |cat
Enter passphase: <type any 8 characters>
98e3f39c54becc9999c2fd4a0b440fdbb74a19e785dada3a46b7ef46301542ff
```
8 characters is the minimum that **pbkdf2** accepts. If you need less, you
need to edit #define MIN_PASSPHRASE_LEN in *pbkdf2.c*.
The output is piped to *cat* command because **pbkdf2** refuses to write the
password on a *tty*.

The output is 64 characters in hexadecimal ascii representing a 256 bit
integer. You will get the exact same integer in raw format with -r option:
```
$ pbkdf2 -r saltfile |hexdump -C
Enter passphrase: <type the same password as before>
00000000  98 e3 f3 9c 54 be cc 99  99 c2 fd 4a 0b 44 0f db  |....T......J.D..|
00000010  b7 4a 19 e7 85 da da 3a  46 b7 ef 46 30 15 42 ff  |.J.....:F..F0.B.|
```
Before careful when generating binary keys and piping them to other tools
because some tools might stop processing the key short at linefeed \'\n\' or
other character. An ASCII key is probably safer.
