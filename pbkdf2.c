/*
 * pbkdf2
 *
 * Copyright 2013 Heikki Orsila
 *
 * SPDX short identifier: BSD-2-Clause
 *
 * File issues at https://gitlab.com/heikkiorsila/pbkdf2
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/select.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#define die(fmt, args...) do { \
	fprintf(stderr, "pbkdf2: " fmt, ## args); \
	exit(1); \
} while(0)

#define KEY_SIZE 32
#define DEFAULT_ITERATIONS 1000000
#define MIN_ITERATIONS 10000
#define MIN_PASSPHRASE_LEN 8

struct secure {
	size_t saltlen;
	size_t passlen;
	unsigned long iterations;
	char salt[256];
	char pass[256];
	char key[KEY_SIZE];
	char asciikey[2 * KEY_SIZE + 1];
};

static struct secure *allocate_secure_memory(void)
{
	void *memory;
	struct secure *secret;
	/*
	 * Allocate extra 4096 bytes for an extra page in
	 * memory for mlock syscall
	 */
	size_t secretsize = sizeof(secret[0]) + 4096;
	if (posix_memalign(&memory, 4096, secretsize))
		die("Could not allocate aligned memory\n");
	secret = memory;
	memory = NULL;
	memset(secret, 0, sizeof secret[0]);

	if (mlock(secret, sizeof secret[0]))
		die("Can not lock secret into physical memory\n");

	return secret;
}

static void validate_crypto_parameters(const struct secure *input)
{
	if (KEY_SIZE < 16 || KEY_SIZE > 512)
		die("KEY_SIZE must be in range [16, 512]\n");
	if (input->iterations < MIN_ITERATIONS)
		die("Iterations must be at least %d\n", MIN_ITERATIONS);
}

static ssize_t read_all(int fd, void *buf, size_t maxcount)
{
        char *b = buf;
        size_t bytes_read = 0;
        ssize_t ret;
        while (bytes_read < maxcount) {
                ret = read(fd, &b[bytes_read], maxcount - bytes_read);
                if (ret < 0) {
                        if (errno == EINTR)
                                continue;
                        if (errno == EAGAIN) {
                                fd_set s;
                                FD_ZERO(&s);
                                FD_SET(fd, &s);
                                select(fd + 1, &s, NULL, NULL, NULL);
                                continue;
                        }
                        return -1;
                } else if (ret == 0) {
                        return bytes_read;
                }
                bytes_read += ret;
        }
        return bytes_read;
}

static ssize_t write_all(int fd, const void *buf, size_t count)
{
	char *b = (char *) buf;
	size_t bytes_written = 0;
	ssize_t ret;
	while (bytes_written < count) {
		ret = write(fd, &b[bytes_written], count - bytes_written);
		if (ret < 0) {
			if (errno == EINTR)
				continue;
			if (errno == EAGAIN) {
				fd_set s;
				FD_ZERO(&s);
				FD_SET(fd, &s);
				select(fd + 1, &s, NULL, NULL, NULL);
				continue;
			}
			if (bytes_written > 0)
				return bytes_written;
			return -1;
		} else if (ret == 0) {
			return bytes_written;
		}
		bytes_written += ret;
	}
	return bytes_written;
}

static void read_salt(struct secure *input, const char *saltfile)
{
	int fd = open(saltfile, O_RDONLY);
	if (fd < 0)
		die("Can't open salt file %s: %s\n", saltfile, strerror(errno));

	input->saltlen = read_all(fd, input->salt, sizeof input->salt);
	if (input->saltlen == (size_t) -1)
		die("Could not read data from %s: %s\n",
		    saltfile, strerror(errno));

	if (close(fd))
		die("Can not close fd: %s\n", strerror(errno));

	if (input->saltlen < 8)
		die("SALT must be at least 8 bytes\n");
	if (input->saltlen >= sizeof input->salt)
		die("SALT too long\n");
}

static void restore_terminal(const struct termios *terminal, int fd)
{
	tcsetattr(fd, TCSANOW, terminal);
}

int no_terminal_echo(struct termios *old_settings, int fd)
{
	struct termios new_settings;
	if (!isatty(fd))
		return 0;

	if (tcgetattr(fd, old_settings))
		die("Reading terminal settings failed: %s\n", strerror(errno));

	new_settings = *old_settings;
	new_settings.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL);

	if (tcsetattr(fd, TCSAFLUSH, &new_settings))
		die("Can not setup terminal: %s\n", strerror(errno));

	return 1;
}

/*
 * Read one line or up to maxcount bytes, whichever comes first.
 *
 * If error occurs during reading, return -1.
 * If newline ('\n') is detected, returns the number of bytes before newline.
 * If newline is not detected, returns the number of bytes read.
 */
static ssize_t read_line(int fd, const void *buf, size_t maxcount)
{
        char *b = (char *) buf;
        size_t bytes_read = 0;
        ssize_t ret;
	size_t i;
        while (bytes_read < maxcount) {
                ret = read(fd, &b[bytes_read], maxcount - bytes_read);
                if (ret < 0) {
                        if (errno == EINTR)
                                continue;
                        if (errno == EAGAIN) {
                                fd_set s;
                                FD_ZERO(&s);
                                FD_SET(fd, &s);
                                select(fd + 1, &s, NULL, NULL, NULL);
                                continue;
                        }
                        return -1;
                } else if (ret == 0) {
                        return bytes_read;
                }

		for (i = 0; i < (size_t) ret; i++) {
			if (b[bytes_read + i] == '\n')
				break;
		}
		assert(i <= (size_t) ret);
                bytes_read += i;
		/* Break loop if newline was found */
		if (i < (size_t) ret)
			break;
        }
        return bytes_read;
}

/* Reads a new line ('\n') terminated password */
static void read_passphrase(struct secure *input)
{
	size_t pass_buf_size = sizeof(input->pass);
	size_t ret;
	struct termios terminal;
	int is_terminal;
	int tty_fd;

	tty_fd = open("/dev/tty", O_RDONLY);
	if (tty_fd < 0)
		die("Can not open /dev/tty: %s\n", strerror(errno));

	is_terminal = no_terminal_echo(&terminal, tty_fd);

	if (is_terminal) {
		fprintf(stderr, "Enter passphrase: ");
		fflush(stderr);
	}

	ret = read_line(tty_fd, input->pass, pass_buf_size);

	if (is_terminal) {
		/* Restore terminal settings before proceeding */
		fprintf(stderr, "\n");
		restore_terminal(&terminal, tty_fd);
	}

	if (ret == (size_t) -1)
		die("Read failed: %s\n", strerror(errno));
	assert(ret <= pass_buf_size);
	if (ret >= (pass_buf_size - 1))
		die("Password too long\n");
	assert(input->passlen < pass_buf_size);

	input->passlen = ret;
	input->pass[input->passlen] = 0;

	if (input->passlen < MIN_PASSPHRASE_LEN)
		die("Passphrase too short\n");
}

static void derive_key(struct secure *output, const struct secure *input)
{
	gpg_error_t err;
	err = gcry_kdf_derive(input->pass, input->passlen,
			      GCRY_KDF_PBKDF2, GCRY_MD_SHA256,
			      input->salt, input->saltlen,
			      input->iterations,
			      sizeof output->key, output->key);
	if (err != 0)
		die("gcrypt error code %d: %s\n", err, gcry_strerror(err));
}

static int to_hex(int c)
{
	assert(c >= 0 && c < 16);
	if (c < 10)
		return '0' + c;
	else
		return 'a' + c - 10;
}

ssize_t xwrite(int fd, const void *buf, size_t count)
{
	char *b = (char *) buf;
	size_t bytes_written = 0;
	ssize_t ret;
	while (bytes_written < count) {
		ret = write(fd, &b[bytes_written], count - bytes_written);
		if (ret < 0) {
			if (errno == EINTR)
				continue;
			if (errno == EAGAIN) {
				fd_set s;
				FD_ZERO(&s);
				FD_SET(fd, &s);
				select(fd + 1, NULL, &s, NULL, NULL);
				continue;
			}
			return -1;
		}
		bytes_written += ret;
	}
	return bytes_written;
}

static void write_key(struct secure *output, int raw_mode)
{
	size_t i;
	size_t ret;
	const char *write_data;
	size_t write_size;

	if (isatty(1))
		die("Refusing to write the derived key on a terminal stdout\n");

	if (raw_mode) {
		write_data = output->key;
		write_size = sizeof output->key;
	} else {
		for (i = 0; i < sizeof output->key; i++) {
			size_t j = 2 * i;
			int c;
			assert((j + 1) < sizeof output->asciikey);

			c = to_hex((output->key[i] & 0xf0) >> 4);
			output->asciikey[j + 0] = c;

			c = to_hex(output->key[i] & 0xf);
			output->asciikey[j + 1] = c;
		}
		i = 2 * KEY_SIZE;
		assert(i < sizeof output->asciikey);
		output->asciikey[i] = '\n';
		write_data = output->asciikey;
		write_size = sizeof output->asciikey;
	}

	ret = xwrite(1, write_data, write_size);
	if (ret != write_size)
		die("Did not write the whole key\n");
}

static void usage(void)
{
	fprintf(stderr,
"pbkdf2 generates SHA-256 hashes on command-line. It is useful for generating\n"
"crypto keys with relatively short passphrases.\n"
"\n"
"For example, on AMD Ryzen Threadripper 1950X, using pbkdf2 with a million \n"
"iterations, it takes approx 0.6s to try one password. Trying all\n"
"12 character passwords from [a-zA-Z0-9] would take approximately\n"
"6e13 computation years with a single thread by following this calculation\n"
"with Python:\n"
"\n"	
">>> '{:e}'.format(pow(62,12) * 0.6 / (3600 * 24 * 365))\n"
"'6.138255e+13'\n"
"\n"
"USAGE:\n"
"\n"
"    pbkdf2 [-i ITERATIONS] [-p] [-r] SALTFILE\n"
"\n"
"where\n"
"\n"
" -i    Set number of iterations for pbkdf2.\n"
"       The default is 1000000 (million) iterations.\n"
"       The higher the value the longer it takes to compute one hash.\n"
" -p    Set passthrough mode. Password is first read from /dev/tty, and the\n"
"       password is written to stdout. Then, all data from stdin is read and\n"
"       written to stdout. You can use this option to give a password for\n"
"       gpg. See EXAMPLE section.\n"
" -r    Set raw mode. Key is printed to stdout in raw\n"
"       (non-hexadecimal ascii) format. By default, key is printed in\n"
"       hexadecimal format.\n"
"\n"
"SALTFILE should be a unique file so that no one else has the same file.\n"
"This is to prevent precalculated dictionary attacks.\n"
"The content of the file is not a secret.\n"
"\n"
"EXAMPLE:\n"
"\n"
"    pbkdf2 saltfile |cryptsetup -d - create cryptodevice /dev/sda2\n"
"\n"
"will use pbkdf2 for key derivation and pass it to cryptsetup to create an\n"
"encrypted device.\n"
"\n"
"    tar c files* |pbkdf2 -p saltfile |gpg -c --passphrase-fd 0 > encryptedfile\n"
"    cat encryptedfile |pbkdf2 -p saltfile |gpg -d --passphase-fd 0 > tarball\n"
"will encrypt and decrypt files by using gpg.\n"
);
}

static void discard(struct secure *data)
{
	memset(data, 0, sizeof data[0]);
	free(data);
}

int main(int argc, char *argv[])
{
	struct secure *input = allocate_secure_memory();
	struct secure *output = allocate_secure_memory();
	const char *saltfile;
	int ret;
	int raw_mode = 0;
	long iterations = DEFAULT_ITERATIONS;
	char *endptr;
	int passthrough_mode = 0;

	while (1) {
		ret = getopt(argc, argv, "i:pr");
		if (ret < 0)
			break;
		switch (ret) {
		case 'i':
			errno = 0;
			iterations = strtol(optarg, &endptr, 10);
			if (errno != 0 || *endptr != 0 || iterations <= 0) {
				die("Invalid number of iterations: %s\n",
				    optarg);
			}
			break;

		case 'p':
			passthrough_mode = 1;
			break;

		case 'r':
			raw_mode = 1;
			break;

		default:
			exit(1);
		}
	}

	if (optind == argc) {
		usage();
		die("Must have salt file as a parameter\n");
	}

	saltfile = argv[optind];
	input->iterations = iterations;

	validate_crypto_parameters(input);

	read_salt(input, saltfile);

	read_passphrase(input);

	derive_key(output, input);

	/* input can be destroyed after the key is derived */
	discard(input);
	input = NULL;

	write_key(output, raw_mode);

	discard(output);
	output = NULL;

	if (passthrough_mode) {
		char buf[4096];
		ssize_t bytes;
		ssize_t written;
		while (1) {
			bytes = read_all(0, buf, sizeof buf);
			if (bytes < 0) {
				fprintf(stderr, "Read error on stdin: %s\n",
					strerror(errno));
			} else if (bytes == 0) {
				break;
			}
			written = write_all(1, buf, bytes);
			if (written < 0)
				die("Write error on stdout: %s\n",
				    strerror(errno));
			if (written < bytes)
				die("Short write on stdout: %s\n",
				    strerror(errno));
		}
	}

	return 0;
}
